require "FuGOL/version"
require 'byebug'

module FuGOL
end
class FuGOL::Game
  GRID_SIZE_X = 50
  GRID_SIZE_Y = 80
  def initialize()
    @grid = Array.new(GRID_SIZE_X) { Array.new(GRID_SIZE_Y, 0) }
    gosper
    @iter = 0
    self
  end

  def glider
    flip(0,0)
    flip(1,1)
    flip(1,2)
    flip(2,0)
    flip(2,1)
  end

  def blinker
    flip(0,1)
    flip(1,1)
    flip(2,1)
  end

  def gosper

    flip(1,25)
    flip(2,23)
    flip(2,25)
    flip(3,13)
    flip(3,14)
    flip(3,21)
    flip(3,22)
    flip(3,35)
    flip(3,36)
    flip(4,12)
    flip(4,16)
    flip(4,21)
    flip(4,22)
    flip(4,35)
    flip(4,36)
    flip(5,1)
    flip(5,2)
    flip(5,11)
    flip(5,17)
    flip(5,21)
    flip(5,22)
    flip(6,1)
    flip(6,2)
    flip(6,11)
    flip(6,15)
    flip(6,17)
    flip(6,18)
    flip(6,23)
    flip(6,25)
    flip(7,11)
    flip(7,17)
    flip(7,25)
    flip(8,12)
    flip(8,16)
    flip(9,13)
    flip(9,14)
  end

  def flip(x,y)
    @grid[x][y] = @grid[x][y] == 1 ? 0 : 1
    self
  end

  def live_neighbors(x,y)
    if x == GRID_SIZE_X-1
      x = -1
    end
    if y == GRID_SIZE_Y-1
      y = -1
    end
    #byebug
    @grid[x-1][y-1] +
    @grid[x-1][y]   +
    @grid[x-1][y+1] +
    @grid[x][y-1]   +
    @grid[x][y+1]   +
    @grid[x+1][y-1] +
    @grid[x+1][y]   +
    @grid[x+1][y+1]
  end

  def step
    grid_step = @grid.map {|row| row.dup }
    @grid.each_with_index do |row, x|
      row.each_with_index do |col, y|
        #byebug
        if col == 0 # cell is dead
          #puts "#{x},#{y} was dead"
          if live_neighbors(x,y) == 3
            #puts "  #{x},#{y} has 3 live neighbors and is now alive."
            grid_step[x][y] = 1
          else
            #puts "  #{x},#{y} stays dead."
          end
        else # cell is alive
          #puts "#{x},#{y} was alive."
          if live_neighbors(x,y) > 3 # Overpopulation
            #puts "  #{x},#{y} has more than 3 live neighbors and is now dead."
            grid_step[x][y] = 0
          elsif live_neighbors(x,y) < 2 # Underpopulation
            #puts "  #{x},#{y} has less than 2 live neighbors and is now dead."
            grid_step[x][y] = 0
          else # Stasis
            #puts "  #{x},#{y} stays alive."
          end
        end
      end
    end
    @grid = grid_step
    @iter += 1
    self
  end

  def state
    puts "iteration #{@iter}:\n" + @grid.inject("") { |acc, r| acc + r.map{|x| x == 1 ? '◆' : '◇'}.join('') + "\n\e[2K" }
  end
end
